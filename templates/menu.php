<header class="cabecera-mov">
       <!--Imagenes superior menu-->
       <div class="huincha-desk">
         <img class="img-fluid huincha center-block" src="<? echo base_url();?>img/tira_superior-min.png" alt="Carozzi">
      </div>
      <div class="huincha-cel">
          <div><img class="imagen-arriba" src="<?echo base_url();?>/img/huincha_superior-min.png" alt=""></div>
     </div>   
       <!--Fin Imagenes superior menu-->
       <!--Menu Desktop-->
       <div class="cabeza desk">
            <nav class="navbar navbar-container navbar-expand-lg">
	        <div class="container-fluid">
		    <div class="navbar-header">
		    	<button class="navbar-toggler" data-toggle="collapse" data-target="#myNavbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    		<img src="<?echo base_url();?>/img/menu.png">
                </button>
		    </div>
		    <div class="collapse navbar-collapse colapsao" id="myNavbar">
		    	<ul class="nav navbar-nav mx-auto">
		    		<li><a href="<? echo base_url();?>">INICIO</a></li>
		    		<li class="subt">
                        <a href="<?echo base_url();?>historia">MARCAS</a>
                        <ul class="sub-menu animated faster">
                          <div class="row">
                            <div class="col-lg-4 borde-hist"><li><a href="<?echo base_url();?>historia">HISTORIA</a></li></div>
                            <div class="col-lg-4 borde-hist"><li><a href="<?echo base_url();?>calidad" >CALIDAD</a></li></div>
                            <div class="col-lg-4 "><li><a href="<?echo base_url();?>deportes">DEPORTES</a></li></div>  
                          </div>  
                        </ul>
                    </li>
		    		<li class="subt">
                        <a href="<?echo base_url();?>recetas">RECETAS</a>
                        <ul class="sub-menu animated faster">
                            <div class="row">
                                <div class="col-lg-3 borde-hist"><li><a href="ocasiones">OCASIONES ESPECIALES</a></li></div>
                                <div class="col-lg-3 borde-hist"><li><a href="tipo-de-pasta">POR TIPO DE PASTA</a></li></div>
                                <div class="col-lg-3 borde-hist"><li><a href="salsa">SALSA</a></li></div>
                                <div class="col-lg-3"><li><a href="todas">TODAS</a></li></div>
                            </div>
                        </ul>
                    </li>
		    		<li class="subt">
                        <a href="#">COCINA CAROZZI</a>
                        <ul class="sub-menu animated faster">
                            <div class="row">
                                <div class="col-lg-6 borde-hist"><li><a href="<?echo base_url();?>glosario">GLOSARIO</a></li></div>
                                <div class="col-lg-6 borde-hist"><li><a href="<?echo base_url();?>datos-culinarios">DATOS CULINARIOS</a></li></div>
                            </div>
                        </ul>
                    </li>
                    <li class="subt">
                        <a href="<?echo base_url();?>productos">PRODUCTOS</a>
                        <ul class="sub-menu animated faster">
                            <div class="row">
                                <div class="col-lg-3 borde-hist"><li><a href="pastas">PASTAS</a></li></div>
                                <div class="col-lg-3 borde-hist"><li><a href="acompanamientos">ACOMPAÑAMIENTOS</a></li></div>
                                <div class="col-lg-3 borde-hist"><li><a href="<?echo base_url();?>productos/nuestros-envases">NUESTROS ENVASES</a></li></div>
                                <div class="col-lg-3"><li><a href="pomarola">POMAROLA</a></li></div>
                            </div>
                        </ul>
                    </li>
                    <li><a href="<?echo base_url();?>contacto">CONTACTO</a></li>
		    	</ul>
		    </div>
	        </div>
            </nav>
      </div>
      <!--Fin Menu Desktop-->
   </header>