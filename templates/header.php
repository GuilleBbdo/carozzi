<!DOCTYPE html>
<html>
<head>
	<? /* ZONA DE MODIFICACIONES  */ ?>

	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?echo $SEOtitulo;?></title>

	<link rel="shortcut icon" href="<? echo base_url()?>img/icono.png" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/bootstrap4/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/styles.css">
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/animate.css">
	<link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
	<style>
		.subt{
			z-index:99;
		}
	</style>	
	
	<? /* fin zona modificaciones */?>
</head>
<body>
	