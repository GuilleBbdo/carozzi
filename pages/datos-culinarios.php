<link rel="stylesheet" href="<? echo base_url();?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/culinarios.css">

<img src="<? echo base_url();?>/img/culinarios-min.png" class="img-fluid img-cabecera" alt="">
<h1 class="tit-culinario">Datos Culinarios</h1>
<p class="p-culinario">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud<br>
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br>
 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

 <!--Carrusel Equivalencias-->
 <div class="owl-carousel owl-theme" id="carrusel-culinario1">
  <div class="item">
      <div class="row equivalencias">
        <div class="col-lg-4 letra-eq col-sm-12">
          <div class="letra-eq align-items-center">
            <h1 class="letras-equiv">Tabla de</h1>
            <h1 class="letras-equiv">equivalencia</h1>
            <h1 class="solidos">Solidos</h1>
          </div>
        </div>
        <div class="col-lg-8 col-sm-12">
                <div class="contiene-tabla"><img src="<?echo base_url();?>/img/tabla-min.png" alt=""></div>
        </div>
      </div>
  </div>
  <div class="item">
    <div class="row equivalencias">
        <div class="col-lg-4 letra-eq col-sm-12">
          <div class="letra-eq align-items-center">
            <h1 class="letras-equiv">Tabla de</h1>
            <h1 class="letras-equiv">equivalencia</h1>
            <h1 class="solidos">Solidos</h1>
          </div>
        </div>
        <div class="col-lg-8 col-sm-12">
                <div class="contiene-tabla"><img src="<?echo base_url();?>/img/tabla-min.png" alt=""></div>
        </div>
      </div>
  </div>
  <div class="item"><div class="row equivalencias">
        <div class="col-lg-4 letra-eq col-sm-12">
          <div class="letra-eq align-items-center">
            <h1 class="letras-equiv">Tabla de</h1>
            <h1 class="letras-equiv">equivalencia</h1>
            <h1 class="solidos">Solidos</h1>
          </div>
        </div>
        <div class="col-lg-8 col-sm-12">
                <div class="contiene-tabla"><img src="<?echo base_url();?>/img/tabla-min.png" alt=""></div>
        </div>
      </div>
  </div>
</div>

<!-- Fin Carrusel Equivalencias-->

<!--Carrusel Tiempos de Coccion-->
<div class="owl-carousel owl-theme" id="carrusel-culinario2">
  <div class="item">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 class="tiempos">Tiempos de cocción /carnes</h1>
        </div>
        <div class="col-sm-12">
          <div class="table-responsive">
              <table class="table table-borderless tabla-coccion">
                  <thead>
                    <tr class="tr-tabla">
                      <th scope="col" class="bordecito">carne</th>
                      <th scope="col" class="bordecito">plancha</th>
                      <th scope="col" class="bordecito">horno</th>
                      <th scope="col" class="bordecito">sartén</th>
                      <th scope="col" class="bordecito">cocido</th>
                    </tr>
                  </thead>
                  <tbody class="tbody-coccion">
                    <tr>
                      <td class="plancha">Vacuno</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td>1 hora 10 minutos</td>
                    </tr>
                    <tr>
                      <td class="plancha">Pollo</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td>1 hora 10 minutos</td>
                    </tr>
                    <tr>
                      <td class="plancha">Pato</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td >1 hora 10 minutos</td>
                    </tr>
                    <tr>
                      <td class="plancha">Cordero</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td>1 hora 10 minutos</td>
                    </tr>
                    <tr>
                      <td class="plancha">Conejo</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td>1 hora 10 minutos</td>
                    </tr>
                    <tr>
                      <td class="plancha">Cerdo</td>
                      <td class="plancha">10 a 12 minutos</td>
                      <td class="plancha">1 hora</td>
                      <td class="plancha">10 minutos</td>
                      <td>1 hora 10 minutos</td>
                    </tr>
                  </tbody>
              </table>
            </div>
          </div>
      </div>    
    </div>
  </div>
</div>
<!--Fin Carrusel Tiempos de Coccion-->