<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/nuestros_envases.css">
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/animate.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.theme.default.min.css">

<div class="titulo-nuestros-envases">
	
	<h1>CONOCE NUESTROS ENVASES</h1>
</div>
	<div class="contendor-titulo-envase">
				      			
				      			<div class="flecha-izquierda">
				      				<img src="<?echo base_url();?>/img/flecha_izq.png" class="img-fluid" alt="">
				      			</div>
				      			<div class="titulo">
				      				<h1>PASTAS CORTAS</h1>
				      			</div>
				      			<div class="flecha-derecha">
				      				
				      				<img src="<?echo base_url();?>/img/flecha_der.png" class="img-fluid" alt="">
				      				
				      			</div>
				      		

				      	</div>

<div class="container-nuestros-envases">	
			<div class="carrusel">
				<div class="owl-carousel owl-theme" id="carrusel-nuestros-envases">
				      <div class="item animated fadeIn" >



				      
				      	<div class="row">
				      		
							<div class="col-lg-3 col-md-12 contenedor-tips order-12-mod">
								<div class="ttips arriba  animated zoomInLeft faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>
								<div class="ttips abajo  animated zoomInLeft faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>

							</div>
							<div class="col-lg-6 col-md-12 contenedor-imagen">
								
									<img src="<?echo base_url();?>/img/productos/envases/producto-1.png" class="img-fluid imagen-producto"  alt="">
							</div>

							<div class="col-lg-3 col-md-12 contenedor-tips">
									<div class="ttips arriba  animated zoomInRight faster ">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>
								<div class="ttips abajo  animated zoomInRight faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>

							</div>


				      	</div>
				      	
				      </div>

				      <div class="item">



				      
				      	<div class="row">
				      		
							<div class="col-lg-3 col-md-12 contenedor-tips order-12-mod">
								<div class="ttips arriba  animated zoomInLeft faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>
								<div class="ttips abajo  animated zoomInLeft faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>

							</div>
							<div class="col-lg-6 col-md-12 contenedor-imagen">
								
									<img src="<?echo base_url();?>/img/productos/envases/producto-1.png" class="img-fluid imagen-producto"  alt="">
							</div>

							<div class="col-lg-3 col-md-12 contenedor-tips">
									<div class="ttips arriba  animated zoomInRight faster ">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>
								<div class="ttips abajo  animated zoomInRight faster">
				
									<h1 class="nombre">
										Dolem ipsum dolor set amen
									</h1>
									<h2 class="descripcion">Dolem ipsum dolor set amen Dolem ipsum dolor set amen Dolem ipsum dolor set amen</h2>
								</div>

							</div>


				      	</div>
				      	
				      </div>
					</div>
				</div>
</div>


	



