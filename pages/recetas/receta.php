
<? //echo "<pre>";print_r( $receta ) ; exit();
   $nombreReceta = explode(" ", $receta->receta_nombre);


   $preparacion = explode("/(2\.)/", $receta->preparacion);


    //echo "<pre>";print_r( $preparacion ) ; exit();
?>

<link rel="stylesheet" href="<? echo base_url();?>css/interior.css">

<img src="<? echo base_url();?>/img/int-receta-min.png" class="img-fluid img-cabecera" alt="">

<div class="container">
    <div class="titulo-interior">
        <h1 class="no-borde"><? echo $nombreReceta[0]; ?>&nbsp;<span class="bordeada"><? for($i = 1; $i <= (count($nombreReceta)-1); $i++){ echo $nombreReceta[$i]. " "; } ?></span></h1>
    </div>
    <div class="calificacion">
        <i class="far fa-star" data-calificacion="1"></i>
        <i class="far fa-star" data-calificacion="2"></i>
        <i class="far fa-star" data-calificacion="3"></i>
        <i class="far fa-star" data-calificacion="4"></i>
        <i class="far fa-star" data-calificacion="5"></i>
        <span class="cantidad"><? echo $receta->receta_calificacion; ?>/5</span>
        <span class="votos"><? echo $receta->receta_cantidad_votos; ?> Votos</span>
    </div>
    <div class="row detalles">
        <div class="col det-1"><span>PARA : <? echo $receta->receta_cantidad_comensales; ?> PERSONAS </span><span class="slash">/</span></div>
        <div class="col det-1"><span>PREPARACIÓN: <? echo strtoupper($receta->receta_tiempo_preparacion); ?></span><span class="slash">/</span></div>
        <div class="col det-1"><span>DIFICULTAD: <? echo strtoupper($receta->receta_dificultad); ?></span></div>
    </div>
    <h5 class="tit-ing">Ingredientes:</h5>
    <div class="row ingre">
        <? 
            $ingredientes = preg_split('/\r\n|\r|\n/',$receta->receta_ingredientes);
            foreach($ingredientes as $key => $ing){
        ?>
        <div class="ingred"><?=$ing?> <span class="rayita-mob">|</span></div>
        <?
            }
        ?>
    </div>
    <div class="row instrucciones">
        <? 
            $preparacionReceta = preg_split('/\r\n|\r|\n/',$receta->preparacion);
            foreach($preparacionReceta as $key => $rec){             
        ?>
        <div class="col"><img src="<?echo base_url();?>/img/foto-rec-min.png" alt="">
            <h1 class="numero"><?=$key+1;?>.</h1>
            <p class="descrip"><?=$rec;?></p>
        </div>
        <?
            }
        ?>
    </div>
</div>

<script type="text/javascript" src="<?=base_url()?>js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/receta.js"></script>