<link rel="stylesheet" href="<? echo base_url();?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/deporte.css">

    <div class="owl-carousel owl-theme" id="carrusel-deporte">
        <div class="item"><h1 class="titulo-dep">DEPORTE</h1><p class="texto-centro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br>veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate<br>velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id<br>est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p><img src="<?echo base_url();?>/img/deporte-img-min.png" class="img-fluid" alt=""></div>
        <div class="item"><h1 class="titulo-dep">DEPORTE</h1><p class="texto-centro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br>veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate<br>velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id<br>rest laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p><img src="<?echo base_url();?>/img/deporte-img-min.png" class="img-fluid" alt=""></div>
        <div class="item"><h1 class="titulo-dep">DEPORTE</h1><p class="texto-centro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br>veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate<br>velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id<br>est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p><img src="<?echo base_url();?>/img/deporte-img-min.png" class="img-fluid" alt=""></div>
    </div>
    <div class="container deportistas">
      <div class="row valentina">
        <div class="col-lg-6 col-md-6">
          <img class="img-deportistas" class="img-fluid" src="<? echo base_url();?>/img/valentina-min.jpg" alt="">
        </div>
        <div class="col-lg-6 col-md-6 valentina-col d-flex align-items-center">
            <h1 class="tit-dep">Valentina Carvallo</h1>
            <h6 class="tit-dep2">Atleta Chilena</h6>
            <p class="parraf-dep">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br>
             magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br>
             commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla<br>
             pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est<br>
             laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,<br>
             totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt<br>
             explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
        </div>
      </div>
      <div class="row lucas">
          <div class="col-lg-6 col-md-6 lucas-col d-flex align-items-center">
          <h1 class="tit-dep">Lucas Jaramillo</h1>
              <h6 class="tit-dep2">Atleta Chileno</h6>
              <p class="parraf-dep">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br>
               magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br>
               commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla<br>
               pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est<br>
               laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,<br>
               totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt<br>
               explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
          </div>
          <div class="col-lg-6 col-md-6 lucas-col">
               <img class="img-deportistas" src="<? echo base_url();?>/img/lucas-min.jpg" alt="">
          </div>
      </div>
      <div class="row sol">
        <div class="col-lg-6 col-md-6">
          <img class="img-deportistas" src="<? echo base_url();?>/img/sol-min.jpg" alt="">
        </div>
        <div class="col-lg-6 col-md-6 sol-col d-flex align-items-center">
            <h1 class="tit-dep">Sol  Ottenhsimer</h1>
            <h6 class="tit-dep2">Atleta Chilena</h6>
            <p class="parraf-dep">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore<br>
             magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br>
             commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla<br>
             pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est<br>
             laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,<br>
             totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt<br>
             explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
        </div>
      </div>  
    </div>
    <div class="row texto-sup">
          <div class="col-lg-4 col-md-6 col-sm-12 pmovil d-flex align-items-center">
          <h1 class="titulo-pmovil">PASTA MOVIL</h1>
                <p class="sub-pmovil">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do<br>
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim<br>
                ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut<br>
                aliquip ex ea commodo consequat. Duis aute irure dolor in<br>
                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla<br>
                pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa<br>
                qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis<br> 
                </p>
          </div>
          <div class="col-lg-8  col-md-6 col-sm-12  imagen-pmovil">
              
          </div>
    </div>