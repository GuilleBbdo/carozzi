<link rel="stylesheet" href="<? echo base_url();?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/glosario.css">

<img src="<? echo base_url();?>/img/glosario1-min.png" class="img-fluid img-cabecera" alt="">
<h1 class="tit-glosario">Glosario</h1>
<p class="p-glosario">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud<br>
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br>
 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

 <?php for($i=0;$i<=4;$i++){
     
     echo ('<div class="container">
 <div class="row ">
    <div class="col-lg-3 col-glosario">
        <div class="caja">
            <span class="texto-interno">Aromatizar</span>
        </div>
        <p class="p-glosario">Añadir a alguna preparación, condimentos o alimentos con sabor y olor fuerte</p>
    </div>
    <div class="col-lg-3 col-glosario">
        <div class="caja">
            <span class="texto-interno">Asar</span>
        </div>
        <p class="p-glosario">Cocinar al horno o a la parrilla para que el exterior quede dorado y el interior, jugoso.</p>
    </div>
    <div class="col-lg-3 col-glosario">
        <div class="caja">
            <span class="texto-interno">Al dente</span>
        </div>
        <p class="p-glosario">Expresión para indicar que la pasta está en su punto exacto de cocción, que tiene firmeza y resistencia al diente cuando se come. También se utiliza para las verduras y el arroz.</p>
    </div>
    <div class="col-lg-3 col-glosario">
        <div class="caja">
            <span class="texto-interno">Adobo</span>
        </div>
        <p class="p-glosario">Caldo o salsa hecha con vinagre o vino, aceite, sal, ajo, hierbas aromáticas y condimentos. Sirve para sazonar carnes y pescados. Un ejemplo de adobos son la marinada, el escabeche, etc.</p>
    </div> 
  </div>
</div>');
     
 }?>


