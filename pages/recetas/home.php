<?
	
?>

<link rel="stylesheet" href="<? echo base_url();?>css/recetas.css">

<div class="container">


	<nav class="nav menu-rectangulos">
	  	<a class="nav-link rect-sup" href="#" dato-filtro="1">PICOTEO</a>
	  	<a class="nav-link rect-sup" href="#" dato-filtro="2">PARA COMPARTIR</a>
	  	<a class="nav-link rect-sup" href="#" dato-filtro="3">PARA DOS</a>
	  	<a class="nav-link rect-sup" href="#" dato-filtro="4">CUMPLEAÑOS</a>
	  	<a class="nav-link rect-sup" href="#" dato-filtro="5">ANIVERSARIOS</a>
	  	<a class="nav-link rect-sup" href="#" dato-filtro="6">CELEBRACIONES</a>
	</nav>

	<nav class="mobile">
		<label class="dropdown">
			<div class="dd-button">FILTROS</div>

  		<input type="checkbox" class="dd-input" id="test">
			<ul class="dd-menu">
				<li><a href="" dato-filtro="1">PICOTEO</a></li>
				<li><a href="" dato-filtro="2">PARA COMPARTIR</a></li>
				<li><a href="" dato-filtro="3">PARA DOS</a></li>
				<li><a href="" dato-filtro="4">CUMPLEAÑOS</a></li>
				<li><a href="" dato-filtro="5">ANIVERSARIOS</a></li>
				<li><a href="" dato-filtro="6">CELEBRACIONES</a></li>
			</ul>
		</label>	
	</nav>
	<nav class="dpdn">
		<label class="dropdown">
			<div class="dd-button">
  			  ORDENAR POR
  			</div>

  			<input type="checkbox" class="dd-input" id="test">

  			<ul class="dd-menu">
  			  <li>Edad</li>
  			  <li>Porciones</li>
  			  <li>Etc</li>
  			</ul>
		</label>
	</nav>
	
	<div class="row recetasContainer">
		<?
			foreach($recetasAll as $key => $rec){
		?>
		<div class="col-lg-4" id="<?=$rec->receta_slug?>" data-filtros=",<?=$rec->recetas_filtros?>," >
			<div class="contenedorReceta">
				<img src="<?echo base_url(). $rec->recetas_imagen_thumb;?>" alt="">
				<div class="valoracion" data-ranking="<?=$rec->receta_calificacion?>">
					<button data-calificacion="1"><i class="fas fa-star"></i></button>
					<button data-calificacion="2"><i class="fas fa-star"></i></button>
					<button data-calificacion="3"><i class="fas fa-star"></i></button>
					<button data-calificacion="4"><i class="fas fa-star"></i></button>
					<button data-calificacion="5"><i class="fas fa-star"></i></button>
				</div>
				<div class="nombre-rec"><span><?echo $rec->receta_nombre?></span></div>
				<div class="ver-rec"><a href="<?=base_url()?>recetas/<?=$rec->receta_slug?>" class="btn btn-success">Ver Receta</a></div>
		   	</div>	
		</div>
		<?
			}
		?>
	</div>
</div>
