<link rel="stylesheet" href="<? echo base_url();?>css/producto_interior.css">

<div class="container parte-arriba">
    <div class="row parte">    
    <div class="col-lg-5">
            <div>
            <img class="img-producto" src="<?echo base_url();?>/img/tallarin.png" alt="">
            </div>
        </div>
        <div class="col-lg-7">
            <h1 class="tit-prod">Corbatitas</h1>
            <h5 class="gram-prod">400 Gramos</h5>
            <p class="parraf-desc">Las Pastas Mix de Carozzi son una buena fuente de fibra dietética y están fortificadas con hierro y vitaminas B1, B2 y B3. Además son productos de bajo aporte de grasa total y muy bajos en sodio.</p>
            <h4 class="tit-preparacion">Preparación</h4>
            <p class="parraf-desc">Vierta la pasta en abundante agua hirviendo con sal a gusto (1 litro de agua por cada 100g de pasta). Mantenga a ebullición suave con olla destapada. Revuelva de vez en cuando. Una vez cocidos, escurra el agua y sirva inmediatamente.</p>
            <div class="row cuadrado-icon">
                <div class="col div-icono"><img src="<? echo base_url();?>/img/reloj.png" alt=""><span class="txt-icono">12 minutos al dente</span></div>
                <div class="col div-icono"><img src="<? echo base_url();?>/img/olla.png" alt=""><span class="txt-icono">1 litro de agua hirviendo</span></div>
                <div class="col div-icono"><img src="<? echo base_url();?>/img/talla.png" alt=""><span class="txt-icono">5 porciones</span></div>
            </div>
        </div>
    </div>
    <div class="row descripciones">
        <div class="col-lg-4">
            <h6 class="tit-otros">Otros productos relacionados</h6>
            <div class="grid"> <!-- contenedor -->
                <div class="foto-peque"><img src="<? echo base_url();?>/img/tallarin.png" alt=""></div> <!-- cada uno de los ítems del grid -->
                <div class="foto-peque"><img src="<? echo base_url();?>/img/tallarin.png" alt=""></div>
                <div class="foto-peque"><img src="<? echo base_url();?>/img/tallarin.png" alt=""></div>
                <div class="foto-peque"><img src="<? echo base_url();?>/img/tallarin.png" alt=""></div>
            </div>
        </div>
        <div class="col-lg-8">
            <h4 class="tit-otros">Información Nutricional</h4>
                <table class="table tablilla">
                         <thead>
                           <tr>
                             <th scope="col"></th>
                             <th scope="col tit-tabla">100g</th>
                             <th scope="col tit-tabla">1 Porción</th>
                             <th scope="col tit-tabla">%VD(*)</th>
                           </tr>
                         </thead>
                         <tbody class="letras">
                           <tr>
                             <th scope="row" class="tit">Energía</th>
                             <td>340</td>
                             <td>340</td>
                             <td>340</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit">Proteínas</th>
                             <td>12,0</td>
                             <td>12,0</td>
                             <td>12,0</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Grasa total(g)</th>
                             <td>2,0</td>
                             <td>2,0</td>
                             <td>2,0</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Grasa saturadas(g)</th>
                             <td>0,5</td>
                             <td>0,5</td>
                             <td>0,5</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Grasa trans(g)</th>
                             <td>0,0</td>
                             <td>0,0</td>
                             <td>0,0</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Colesterol(g)</th>
                             <td>68,4</td>
                             <td>68,4</td>
                             <td>68,4</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Carbohidratos disp.(g)</th>
                             <td>4,4</td>
                             <td>4,4</td>
                             <td>4,4</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Azucares totales(g)</th>
                             <td>4,1</td>
                             <td>4,1</td>
                             <td>4,1</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Fibra dietetica total.(g)</th>
                             <td>1,6</td>
                             <td>1,6</td>
                             <td>1,6</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Fibra soluble.(g)</th>
                             <td>2,5</td>
                             <td>2,5</td>
                             <td>2,5</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Sodio (mg)</th>
                             <td>14,0</td>
                             <td>14,0</td>
                             <td>14,0</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Tiamina (mg)</th>
                             <td>0,9</td>
                             <td>0,9</td>
                             <td>0,9</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Rivoflavina(g)</th>
                             <td>0,3</td>
                             <td>0,3</td>
                             <td>0,3</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Niacina(mg EN)</th>
                             <td>5,7</td>
                             <td>5,7</td>
                             <td>5,7</td>
                           </tr>
                           <tr>
                             <th scope="row" class="tit" >Hierro (mg)</th>
                             <td>30</td>
                             <td>30</td>
                             <td>30</td>
                           </tr>
                         </tbody>
                    </table>
                        <ul class="info-abajo">
                            <li>*%Valores Diarios en base a una dieta de 2.000 Kcal u 8.400 kJ, valores referenciales para un adulto en Chile.</li>
                            <li>** Valor no definido para Chile, referencial Mercosur 13%.</li>
                            <li>*** Valor no definido para Mercosur, Valor referencial en Chile 4%.</li>
                            <li>Fuente: Reglamento Sanitario de los Alimentos, Chile.</li>
                        </ul>
                </div>
        </div>
    </div>
</div>