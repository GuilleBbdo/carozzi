<link rel="stylesheet" href="<? echo base_url();?>css/producto.css">


<div class="container">


	<nav class="nav menu-rectangulos">
	  	<a class="nav-link active rect-sup" href="#">CORTAS</a>
	  	<a class="nav-link rect-sup" href="#">LARGAS</a>
	  	<a class="nav-link rect-sup" href="#">RELLENAS</a>
	  	<a class="nav-link rect-sup" href="#">SIN GLUTEN</a>
	  	<a class="nav-link rect-sup" href="#">INTEGRALES</a>
	  	<a class="nav-link rect-sup" href="#">PARA UNO</a>
	</nav>

	<nav class="mobile">
		<label class="dropdown">
			<div class="dd-button">FILTROS</div>

  		<input type="checkbox" class="dd-input" id="test">
			<ul class="dd-menu">
				<li><a href="">CORTAS</a></li>
				<li><a href="">LARGAS</a></li>
				<li><a href=""></a>RELLENAS</li>
				<li><a href=""></a>SIN GLUTEN</li>
				<li><a href=""></a>INTEGRALES</li>
				<li><a href=""></a>PARA UNO</li>
			</ul>
		</label>	
	</nav>
	<nav class="dpdn">
		<label class="dropdown">
			<div class="dd-button">
  			  ORDENAR POR
  			</div>

  			<input type="checkbox" class="dd-input" id="test">

  			<ul class="dd-menu">
  			  <li>Edad</li>
  			  <li>Porciones</li>
  			  <li>Etc</li>
  			</ul>
		</label>
	</nav>
	
	<?php for($i=0;$i<=3;$i++){
      echo ('<div class="container">
		<div class="row">
				<div class="col-lg-4">
				  <div>
					<div class="cuad-prod"><img src="'.base_url().'/img/corbatas-min.png" alt=""></div>
                        <div class="nombre-prod"><span>Corbatas</span></div>
                        <div class="textito"><span>¡No dejes de probar unos ricos tutos deshuesados y disfruta en familia!<br><br></span></div>
						<div class="ver-prod"><a href="'. base_url(). 'productos/producto" class="btn btn-success">Más información</a></div>
				   	</div>	
				</div>
				<div class="col-lg-4">
					<div>
						<div class="cuad-prod"><img src="'.base_url().'/img/espirales2-min.png" alt=""></div>
                        <div class="nombre-prod"><span>Espirales</span></div>
                        <div class="textito"><span>¡En la variedad esta el gusto!Puedes encontrarlo en distintos formatos, como termoformado, bolsa y en bandeja</span></div>
						<div class="ver-prod"><a href="'. base_url(). 'productos/producto" class="btn btn-success">Más información</a></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div>
						<div class="cuad-prod"><img src="'.base_url().'/img/spaghetti-5-min.png" alt=""></div>						
                        <div class="nombre-prod"><span>Spaghetti 5</span></div>
                        <div class="textito"><span>Rápidas y muy sabrosas, ideal para el picoteo en la parrilla y compartir un momento agradable con los que más quieres.</span></div>
						<div class="ver-prod"><a href="'. base_url(). 'productos/producto" class="btn btn-success">Más información</a></div>
					</div>
				</div>
			</div>
	</div>');
     
}?>
</div>