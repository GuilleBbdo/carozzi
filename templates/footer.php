<? /* trabajar desde  aqui  */ ?>


<!--imagen carozzi centro-->
<div class="divImagen img-fluid"><img class="huinchabajo2" src="<?=base_url()?>img/logo_slogan.png" alt="Carozzi"></div>
<footer class="page-footer font-small cyan darken-3">

<div class="contenedor-footer">
  
  <div class="row">
    

    <div class="col-lg-5 col-md-12 col-12 contenedor-unete">
      
      <span class="unete">Únete a nuestra comunidad</span>

    </div>

    <div class="col-lg-2 col-md-12 col-12 logos-contenedor">
       <a target="_blank" class="logito" href="<?=$urlTW;?>"><img src="<?=base_url()?>img/facebook-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlFB;?>"><img src="<?=base_url()?>img/instagram-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlIG;?>"><img src="<?=base_url()?>img/twitter-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlYT;?>"><img src="<?=base_url()?>img/youtube-icon.png" alt=""></a>
    </div>
    <div class=" col-lg-5 col-md-12 col-12 info">
      <span class="carozzi">© 2015 Carozzi S.A | Mesa Central: (56-2)23776400</span>

    </div>
  </div>

</div>

  <!--
  <div class="container copyright-page">
      <div class="copy1">
          <span class="unete">Únete a nuestra comunidad</span>
          <a target="_blank" class="logito" href="<?=$urlTW;?>"><img src="<?=base_url()?>img/facebook-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlFB;?>"><img src="<?=base_url()?>img/instagram-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlIG;?>"><img src="<?=base_url()?>img/twitter-icon.png" alt=""></a>
          <a target="_blank" class="logito" href="<?=$urlYT;?>"><img src="<?=base_url()?>img/youtube-icon.png" alt=""></a>
          <span class="carozzi">© 2015 Carozzi S.A | Mesa Central: (56-2)23776400</span>
      </div>
    </div>-->
  <img class="img-fluid huincha2" src="<?=base_url()?>img/huincha_superior_larga-min.png" alt="Carozzi">
</footer>
<script src="https://kit.fontawesome.com/ba2c1cbd36.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/AnimOnScroll.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/imagesloaded.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/modernizr.custom.js"></script>
<script type="text/javascript" src="<?=base_url();?>/js/classie.js"></script>


<script>

$(window).on('load, resize', function () {
    var viewportWidth = $(window).width();
    if (viewportWidth >= 992) {
           
      $(".subt").hover(
        function () {
         $(".sub-menu").addClass("fadeInUp");
        },
        function () {
          $(".sub-menu").removeClass("fadeInUp");
        }
      );
 


    }
});




</script>




<?php 

$urlActual = $_SERVER['REQUEST_URI'];
switch ($urlActual) {
  case "/sitios/carozzi2020/historia":
      echo('<script type="text/javascript" src="'.base_url().'/js/historia.js"></script>');
      break;
  case "/sitios/carozzi2020/deportes":
      echo('<script type="text/javascript" src="'.base_url().'/js/deporte.js"></script>');
      break;
  case "/sitios/carozzi2020/datos":
    echo('<script type="text/javascript" src="'.base_url().'/js/culinarios.js"></script>');
    break;
  case "/sitios/carozzi2020/recetas":
    echo('<script type="text/javascript" src="'.base_url().'/js/recetas.js"></script>');
    break;
    case "/sitios/carozzi2020/productos/nuestros-envases":
    echo('<script type="text/javascript" src="'.base_url().'/js/nuestros-envases.js"></script>');
    break;
    case "/sitios/carozzi2020/contacto":
    echo('<script type="text/javascript" src="'.base_url().'/js/InputAffix.js"></script>');
     echo('<script type="text/javascript" src="'.base_url().'/js/contacto.js"></script>');
    break;
    case "/sitios/carozzi2020/":
    echo('<script type="text/javascript" src="'.base_url().'/js/home.js"></script>');
    echo ('<link rel="stylesheet" href="'.base_url().'/css/imagehover.min.css">');
    break; 

  }


?>

<? /* fin zona de trabajo */ ?>
</body>
</html>