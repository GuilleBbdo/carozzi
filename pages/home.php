<? 

$carrusel = array();
/* creo un slide */ 
$carrusel[] = dataSlide("slider2-min.png");
$carrusel[] = dataSlide("slider2-min.png");
$carrusel[] = dataSlide("slider2-min.png");

function dataSlide($imagen,$url="#"){
  $slide = new stdClass();
  $slide->imagen = $imagen;
  $slide->url =$url;
  return $slide;
}

?>
<style>
  
  .grid {
  max-width: 69em;
  list-style: none;
  margin: 30px auto;
  padding: 0;
}

.grid li {
  display: block;
  float: left;
  padding: 7px;
  width: 33%;
  opacity: 0;
}

.grid li.shown,
.no-js .grid li,
.no-cssanimations .grid li {
  opacity: 1;
}

.grid li a,
.grid li img {
  outline: none;
  border: none;
  display: block;
  max-width: 100%;
}

/* Effect 1: opacity */
.grid.effect-1 li.animate {
  -webkit-animation: fadeIn 0.65s ease forwards;
  animation: fadeIn 0.65s ease forwards;
}

@-webkit-keyframes fadeIn {
  0% { }
  100% { opacity: 1; }
}

@keyframes fadeIn {
  0% { }
  100% { opacity: 1; }
}

/* Effect 2: Move Up */
.grid.effect-2 li.animate {
  -webkit-transform: translateY(200px);
  transform: translateY(200px);
  -webkit-animation: moveUp 0.65s ease forwards;
  animation: moveUp 0.65s ease forwards;
}

@-webkit-keyframes moveUp {
  0% { }
  100% { -webkit-transform: translateY(0); opacity: 1; }
}

@keyframes moveUp {
  0% { }
  100% { -webkit-transform: translateY(0); transform: translateY(0); opacity: 1; }
}

/* Effect 3: Scale up */
.grid.effect-3 li.animate {
  -webkit-transform: scale(0.6);
  transform: scale(0.6);
  -webkit-animation: scaleUp 0.65s ease-in-out forwards;
  animation: scaleUp 0.65s ease-in-out forwards;
}

@-webkit-keyframes scaleUp {
  0% { }
  100% { -webkit-transform: scale(1); opacity: 1; }
}

@keyframes scaleUp {
  0% { }
  100% { -webkit-transform: scale(1); transform: scale(1); opacity: 1; }
}

/* Effect 4: fall perspective */
.grid.effect-4 {
  -webkit-perspective: 1300px;
  perspective: 1300px;
}

.grid.effect-4 li.animate {
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform: translateZ(400px) translateY(300px) rotateX(-90deg);
  transform: translateZ(400px) translateY(300px) rotateX(-90deg);
  -webkit-animation: fallPerspective .8s ease-in-out forwards;
  animation: fallPerspective .8s ease-in-out forwards;
}

@-webkit-keyframes fallPerspective {
  0% { }
  100% { -webkit-transform: translateZ(0px) translateY(0px) rotateX(0deg); opacity: 1; }
}

@keyframes fallPerspective {
  0% { }
  100% { -webkit-transform: translateZ(0px) translateY(0px) rotateX(0deg); transform: translateZ(0px) translateY(0px) rotateX(0deg); opacity: 1; }
}

/* Effect 5: fly (based on http://lab.hakim.se/scroll-effects/ by @hakimel) */
.grid.effect-5 {
  -webkit-perspective: 1300px;
  perspective: 1300px;
}

.grid.effect-5 li.animate {
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform-origin: 50% 50% -300px;
  transform-origin: 50% 50% -300px;
  -webkit-transform: rotateX(-180deg);
  transform: rotateX(-180deg);
  -webkit-animation: fly .8s ease-in-out forwards;
  animation: fly .8s ease-in-out forwards;
}

@-webkit-keyframes fly {
  0% { }
  100% { -webkit-transform: rotateX(0deg); opacity: 1; }
}

@keyframes fly {
  0% { }
  100% { -webkit-transform: rotateX(0deg); transform: rotateX(0deg); opacity: 1; }
}

/* Effect 6: flip (based on http://lab.hakim.se/scroll-effects/ by @hakimel) */
.grid.effect-6 {
  -webkit-perspective: 1300px;
  perspective: 1300px;
}

.grid.effect-6 li.animate {
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform-origin: 0% 0%;
  transform-origin: 0% 0%;
  -webkit-transform: rotateX(-80deg);
  transform: rotateX(-80deg);
  -webkit-animation: flip .8s ease-in-out forwards;
  animation: flip .8s ease-in-out forwards;
}

@-webkit-keyframes flip {
  0% { }
  100% { -webkit-transform: rotateX(0deg); opacity: 1; }
}

@keyframes flip {
  0% { }
  100% { -webkit-transform: rotateX(0deg); transform: rotateX(0deg); opacity: 1; }
}

/* Effect 7: helix (based on http://lab.hakim.se/scroll-effects/ by @hakimel) */
.grid.effect-7 {
  -webkit-perspective: 1300px;
  perspective: 1300px;
}

.grid.effect-7 li.animate {
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform: rotateY(-180deg);
  transform: rotateY(-180deg);
  -webkit-animation: helix .8s ease-in-out forwards;
  animation: helix .8s ease-in-out forwards;
}

@-webkit-keyframes helix {
  0% { }
  100% { -webkit-transform: rotateY(0deg); opacity: 1; }
}

@keyframes helix {
  0% { }
  100% { -webkit-transform: rotateY(0deg); transform: rotateY(0deg); opacity: 1; }
}

/* Effect 8:  */
.grid.effect-8 {
  -webkit-perspective: 1300px;
  perspective: 1300px;
}

.grid.effect-8 li.animate {
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  -webkit-transform: scale(0.4);
  transform: scale(0.4);
  -webkit-animation: popUp .8s ease-in forwards;
  animation: popUp .8s ease-in forwards;
}

@-webkit-keyframes popUp {
  0% { }
  70% { -webkit-transform: scale(1.1); opacity: .8; -webkit-animation-timing-function: ease-out; }
  100% { -webkit-transform: scale(1); opacity: 1; }
}

@keyframes popUp {
  0% { }
  70% { -webkit-transform: scale(1.1); transform: scale(1.1); opacity: .8; -webkit-animation-timing-function: ease-out; animation-timing-function: ease-out; }
  100% { -webkit-transform: scale(1); transform: scale(1); opacity: 1; }
}

@media screen and (max-width: 900px) {
  .grid li {
    width: 50%;
  }
}

@media screen and (max-width: 400px) {
  .grid li {
    width: 100%;
  }
}

[class*=" imghvr-"] figcaption, [class^=imghvr-] figcaption{
  background-color: #e2241d7a!important;
}
figure {
    display: block;
    margin-block-start: 0px;
    margin-block-end: 0px;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
}
figcaption{
 position: relative;
}
.contenedor-tips{
  position: absolute;
  top: 50%; left: 50%;
  transform: translate(-50%,-50%);
  max-width: 280px;
    width: 100%;
}
.contenedor-tips .logo{
  max-width: 60px;
  text-align: center;
  margin: 0 auto;
      margin-bottom: 10px;
}
.contenedor-tips .titulo h1{
      font-family: volkswagen-serial3,sans-serif;
      font-size: 20px;
      color: white;
}
.contenedor-tips .bajada h2{
      font-family: volkswagen-serial1,sans-serif;
      font-size: 15px;
      color: white;
}
.grid li {
  margin-bottom: 0px;
  padding:0;
  padding: 0px;
  cursor: pointer;
}


</style>
<!-- Carrusel--> 
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
 <ol class="carousel-indicators">
  <?   $active = "active";
      for( $i = 0; $i < count($carrusel); $i++){ ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" class="<?=$active?>"></li>
  <? $active = ""; }?>
 </ol>
 <div class="carousel-inner">
    <?  
     $active = "active";
     foreach ($carrusel as $key => $value) { ?>
      <div class="carousel-item <?=$active?>">
       <img src="<? echo base_url();?>img/slideHome/<?=$value->imagen?>" class="d-block w-100" alt="1">
     </div> 
    <? $active = ""; } ?>
     
     
 </div>
 <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
   <span class="carousel-control-prev-icon" aria-hidden="true"></span>
   <span class="sr-only">Previous</span>
 </a>
 <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
   <span class="carousel-control-next-icon" aria-hidden="true"></span>
   <span class="sr-only">Next</span>
 </a>
</div>
<!-- Fin Carrusel--> 
<!-- Texto central #TipsCarozzi--> 
<div class="d-flex textocentro">
	<div class="textocentradio"><span class="tips">#Tips</span><span class="carozzitip">Carozzi</span></div>
</div>
<!-- Fin Texto central #TipsCarozzi-->
<!-- Contenedor imagenes tips-->
<!--
<div class="gallery container">
  <div class="item2 a"><img src="<? echo base_url();?>img/spaghetti-min.png" alt="spaghetti"></div>
  <div class="item2 b"><img src="<? echo base_url();?>img/jovenes-min.png" alt="jovenes"></div>
  <div class="item2 c"><img src="<? echo base_url();?>img/plato-min.png" alt="plato vacio"></div>
  <div class="item2 d"><img src="<? echo base_url();?>img/polenta-min.png" alt="polenta"></div>
  <div class="item2 e"><img src="<? echo base_url();?>img/sopaletras-min.png" alt="sopa de letras"></div>
  <div class="item2 f"><img src="<? echo base_url();?>img/espirales-min.png" alt="espirales"></div>
  <div class="item2 g"><img src="<? echo base_url();?>img/aji-min.png" alt="aji"></div>
  <div class="item2 h"><img src="<? echo base_url();?>img/boloñesa-min.png" alt="tallarines con bolognesa"></div> 
</div>
-->
<ul class="grid effect-4" id="grid">
<li><figure class="imghvr-fade"><img src="<? echo base_url();?>img/spaghetti-min.png" alt="spaghetti"> 
  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure>
</li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/jovenes-min.png" alt="jovenes">  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/plato-min.png" alt="plato vacio"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/polenta-min.png" alt="polenta"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/sopaletras-min.png" alt="sopa de letras"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/espirales-min.png" alt="espirales"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/aji-min.png" alt="aji"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/boloñesa-min.png" alt="tallarines con bolognesa"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"><img src="<? echo base_url();?>img/spaghetti-min.png" alt="spaghetti"> 
  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure>
</li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/jovenes-min.png" alt="jovenes">  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/plato-min.png" alt="plato vacio"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/polenta-min.png" alt="polenta"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/sopaletras-min.png" alt="sopa de letras"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/espirales-min.png" alt="espirales"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/aji-min.png" alt="aji"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/boloñesa-min.png" alt="tallarines con bolognesa"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"><img src="<? echo base_url();?>img/spaghetti-min.png" alt="spaghetti"> 
  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure>
</li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/jovenes-min.png" alt="jovenes">  <figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/plato-min.png" alt="plato vacio"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/polenta-min.png" alt="polenta"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/sopaletras-min.png" alt="sopa de letras"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/espirales-min.png" alt="espirales"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/aji-min.png" alt="aji"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>
<li><figure class="imghvr-fade"> <img src="<? echo base_url();?>img/boloñesa-min.png" alt="tallarines con bolognesa"><figcaption> 
    <div class="contenedor-tips">
    <div class="logo">
      <img src="<? echo base_url();?>img/Recurso_1.png" alt="" class="img-fluid">
    </div>
    <div class="titulo">
        
        <h1>Este es un tip que te servirá</h1>

    </div>
    <div class="bajada">
      
        <h2>Si tomasmucha agua durante la mañana, te servirá para mantenerte idratado gran parte del día</h2>

    </div>



  </div>
</figcaption></figure></li>

</ul>
<!-- Fin Contenedor imagenes tips-->  