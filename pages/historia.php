<link rel="stylesheet" type="text/css" href="<? echo base_url();?>css/historia.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<? echo base_url();?>css/owl.theme.default.min.css">

<!--Parte nuestra historia -->
        <div class="row texto-sup">
          <div class="col-lg-4 col-md-6 col-sm-12 historia">
          <h1 class="titulo-historia">UNA HISTORIA<br>DE DESAFÍOS</h1>
                <p class="sub-texto">
                Desde 1898, Carozzi se enfrenta al reto de crear una mejor calidad<br>
                de vida poniendo a disposición de sus consumidores todo un universo<br> 
                de productos pensados para disfrutar. Con más de 119 años en la<br> 
                industria alimenticia, Carozzi es considerada hoy una de las<br>
                compañías de consumo masivo más respetadas de Latinoamérica.
                </p>
          </div>
          <div class="col-lg-8  col-sm-12 imagen-historia">
            <img src="<?echo base_url();?>/img/historia/historia2-min.png" class="img-fluid" alt="">
          </div>
        </div>
<!--Parte nuestra historia -->

<!--Carrusel años -->
<div class="owl-carousel owl-theme align-middle" id="carrusel-historia">
  <div class="item"><img src="<?echo base_url();?>/img/foto-slider-min.png" class="img-fluid" alt=""></div>
  <div class="item"><img src="<?echo base_url();?>/img/foto-slider-min.png" class="img-fluid" alt=""></div>
  <div class="item"><img src="<?echo base_url();?>/img/foto-slider-min.png" class="img-fluid" alt=""></div>
</div>
 <!--Carrusel años -->
 <div class="owl-carousel owl-theme" id="carrusel-marca">
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1898-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1898</h5></div>
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1907-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1907</h5></div>
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1910-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1910</h5></div>
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1920-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1920</h5></div>
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1930-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1930</h5></div>
  <div class="item"><div class="contenedor-imagen"><img src="<?echo base_url();?>/img/1940-min.png" class="img-fluid" alt=""></div><br><h5 class="anio">1940</h5></div>
</div>
<!--Carrusel años -->
<!--Slider Historico -->
<div class="row historia-yt">
    <div class="col-md-12">
    <h1 class="titulo-yt">HISTORICOS<br>DE CAROZZI</h1>
    <p class="parraf-yt">Lorem ipsum dolor sit amet consectetur adipisicing elit.<br> 
    Sequi natus dolor quos molestias dolorem assumenda quas rem repellendus,<br>
    esse culpa animi error optio reprehenderit hic aspernatur quidem eveniet maiores doloremque!</p>
    <div class="owl-carousel owl-theme" id="carrusel-youtube">
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1898</h5></div>
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1907</h5></div>
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1910</h5></div>
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1920</h5></div>
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1930</h5></div>
      <div class="item"><iframe width="600" height="285" src="https://www.youtube.com/embed/8ggHjKRGhOg"  modestbranding="1" frameborder="0" controls="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><h5 class="anio2">1940</h5></div>
    </div>
    </div>
</div>
<!--Slider Historico -->
<section id="banner">
    <video id="videobcg" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
        <source src="https://static.lumosity.com/resources/home_page_templates/574/neuralnetwork.mp4" type="video/mp4">
            <source src="movie.webm" type="video/webm">Sorry, your browser does not support HTML5 video.</video>
            <i class="fas fa-play-circle btn-yt"></i>
</section>




